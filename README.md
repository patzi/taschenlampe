# Simples Taschenlampen Programm für die iOS-Plattform
## Features
* Ein-/Ausschalten der Lampe
* Helligkeitssteuerung der Lampe
* nicht mehr/nicht weniger
## Screenshot
![Screenshot des aktuellen Build](https://bytebucket.org/patzi/taschenlampe/raw/791b9562930052272c8e46a7e96ea22064d1f8e0/Screen%20Shot%202016-08-21%20at%2021.01.24.png "iPhone 6 mit 9.3")

## Lizenz

Copyright 2013 patzi

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
