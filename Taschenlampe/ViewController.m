//
//  ViewController.m
//  Taschenlampe
//
//  Created by Nicolas on 06.03.13.
//  Copyright (c) 2013 Small Apps. All rights reserved.
//
//  Copyright 2013 patzi
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize torchButton, torchLevel;

- (void)viewDidLoad{
    [super viewDidLoad];
	if ([AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].hasTorch) {
        torchButton.hidden = false;
    } else {
        torchButton.hidden = true;
    }
    if ([AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff) {
        [torchButton setTitle:@"ON" forState:UIControlStateNormal];
    } else {
        [torchButton setTitle:@"OFF" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)torchButtonPressed:(id)sender {
    if ([AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff) {
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchModeOnWithLevel:torchLevel.value error:nil];
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        [torchButton setTitle:@"OFF" forState:UIControlStateNormal];
    } else {
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
        [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode = AVCaptureTorchModeOff;
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        [torchButton setTitle:@"ON" forState:UIControlStateNormal];
    }
}
- (IBAction)silderChanged:(id)sender {
    if ([AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn) {
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchModeOnWithLevel:torchLevel.value error:nil];
        [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
    }
}
@end
