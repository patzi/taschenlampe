//
//  ViewController.h
//  Taschenlampe
//
//  Created by Nicolas on 06.03.13.
//  Copyright (c) 2013 Small Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *torchButton;
- (IBAction)torchButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *torchLevel;
- (IBAction)silderChanged:(id)sender;

@end
