//
//  AppDelegate.h
//  Taschenlampe
//
//  Created by Nicolas on 06.03.13.
//  Copyright (c) 2013 Small Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
